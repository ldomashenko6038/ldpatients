﻿using LDPatients.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace LDPatients.Controllers
{
    /// <summary>
    ///  The controller handles and responds to user input.
    ///  Depending on requested action controller selects the correct view and provides data using ConcentrationUnit model to it.
    ///  Also, LDConcentrationUnit controller enables all CRUD maintanence.
    /// </summary>
    public class LDConcentrationUnitController : Controller
    {
        private readonly PatientsContext _context;

        public LDConcentrationUnitController(PatientsContext context)
        {
            _context = context;
        }

        // GET: LDConcentrationUnit
        /// <summary>
        /// The method returns all data for ConcentrationUnits model
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Index()
        {
            return View(await _context.ConcentrationUnit.ToListAsync());
        }

        // GET: LDConcentrationUnit/Details/5
        /// <summary>
        /// The method returns details for requested ConcentrationCode
        /// </summary>
        /// <param name="id">ConcentrationCode</param>
        /// <returns>ConcentrationUnit if exists</returns>
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var concentrationUnit = await _context.ConcentrationUnit
                .FirstOrDefaultAsync(m => m.ConcentrationCode == id);
            if (concentrationUnit == null)
            {
                return NotFound();
            }

            return View(concentrationUnit);
        }

        // GET: LDConcentrationUnit/Create
        /// <summary>
        /// The method returns Create New ConcentrationUnit view
        /// </summary>
        /// <returns></returns>
        public IActionResult Create()
        {
            return View();
        }

        // POST: LDConcentrationUnit/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// The method adds concentrationUnit to database
        /// </summary>
        /// <param name="concentrationUnit">ConcentrationUnit object</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ConcentrationCode")] ConcentrationUnit concentrationUnit)
        {
            if (ModelState.IsValid)
            {
                _context.Add(concentrationUnit);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(concentrationUnit);
        }

        // GET: LDConcentrationUnit/Edit/5
        /// <summary>
        /// The method returns edit view with data for requested ConcentrationCode
        /// </summary>
        /// <param name="id">ConcentrationCode</param>
        /// <returns></returns>
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var concentrationUnit = await _context.ConcentrationUnit.FindAsync(id);
            if (concentrationUnit == null)
            {
                return NotFound();
            }
            return View(concentrationUnit);
        }

        // POST: LDConcentrationUnit/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// The method edit requested concentrationUnit and save it in db
        /// </summary>
        /// <param name="id">concentrationCode</param>
        /// <param name="concentrationUnit">concentrationUnit object</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("ConcentrationCode")] ConcentrationUnit concentrationUnit)
        {
            if (id != concentrationUnit.ConcentrationCode)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(concentrationUnit);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ConcentrationUnitExists(concentrationUnit.ConcentrationCode))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(concentrationUnit);
        }

        // GET: LDConcentrationUnit/Delete/5
        /// <summary>
        /// The method returns delete view with ConcentrationUnit model data
        /// </summary>
        /// <param name="id">ConcentrationCode</param>
        /// <returns></returns>
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var concentrationUnit = await _context.ConcentrationUnit
                .FirstOrDefaultAsync(m => m.ConcentrationCode == id);
            if (concentrationUnit == null)
            {
                return NotFound();
            }

            return View(concentrationUnit);
        }

        // POST: LDConcentrationUnit/Delete/5
        /// <summary>
        /// The method deletes the requested ConcentrationCode and save changes in db
        /// </summary>
        /// <param name="id">ConcentrationCode</param>
        /// <returns>Index view for LDConcentrationUnit controller</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var concentrationUnit = await _context.ConcentrationUnit.FindAsync(id);
            _context.ConcentrationUnit.Remove(concentrationUnit);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        /// <summary>
        /// The method checks an existance of ConcentrationCode in ConcentrationUnit model
        /// </summary>
        /// <param name="id">ConcentrationCode</param>
        /// <returns>True if requested ConcentrationCode exists</returns>
        private bool ConcentrationUnitExists(string id)
        {
            return _context.ConcentrationUnit.Any(e => e.ConcentrationCode == id);
        }
    }
}
