﻿using LDPatients.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace LDPatients.Controllers
{
    /// <summary>
    ///  The controller handles and responds to user input.
    ///  Depending on requested action controller selects the correct view and provides data using Country model to it.
    ///  Also, LDCountry controller enables all CRUD maintanence.
    /// </summary>
    public class LDCountryController : Controller
    {
        private readonly PatientsContext _context;

        public LDCountryController(PatientsContext context)
        {
            _context = context;
        }

        // GET: LDCountry
        /// <summary>
        /// The method returns all data for Country model
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Index()
        {
            return View(await _context.Country.ToListAsync());
        }

        // GET: LDCountry/Details/5
        /// <summary>
        /// The method returns details for requested CountryCode
        /// </summary>
        /// <param name="id">CountryCode</param>
        /// <returns>Country if exists</returns>
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var country = await _context.Country
                .FirstOrDefaultAsync(m => m.CountryCode == id);
            if (country == null)
            {
                return NotFound();
            }

            return View(country);
        }

        // GET: LDCountry/Create
        /// <summary>
        /// The method returns Create New Country view
        /// </summary>
        /// <returns></returns>
        public IActionResult Create()
        {
            return View();
        }

        // POST: LDCountry/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// The method adds country to database
        /// </summary>
        /// <param name="country">Country object</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CountryCode,Name,PostalPattern,PhonePattern,FederalSalesTax")] Country country)
        {
            if (ModelState.IsValid)
            {
                _context.Add(country);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(country);
        }

        // GET: LDCountry/Edit/5
        /// <summary>
        /// The method returns edit view with data for requested CountryCode
        /// </summary>
        /// <param name="id">CountryCode</param>
        /// <returns></returns>
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var country = await _context.Country.FindAsync(id);
            if (country == null)
            {
                return NotFound();
            }
            return View(country);
        }

        // POST: LDCountry/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// The method edit requested country and save it in db
        /// </summary>
        /// <param name="id">countryCode</param>
        /// <param name="country">country object</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("CountryCode,Name,PostalPattern,PhonePattern,FederalSalesTax")] Country country)
        {
            if (id != country.CountryCode)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(country);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CountryExists(country.CountryCode))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(country);
        }

        // GET: LDCountry/Delete/5
        /// <summary>
        /// The method returns delete view with Country model data
        /// </summary>
        /// <param name="id">CountryCode</param>
        /// <returns></returns>
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var country = await _context.Country
                .FirstOrDefaultAsync(m => m.CountryCode == id);
            if (country == null)
            {
                return NotFound();
            }

            return View(country);
        }

        // POST: LDCountry/Delete/5
        /// <summary>
        /// The method deletes the requested CountryCode and save changes in db
        /// </summary>
        /// <param name="id">CountryCode</param>
        /// <returns>Index view for LDCountry controller</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var country = await _context.Country.FindAsync(id);
            _context.Country.Remove(country);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        /// <summary>
        /// The method checks an existance of CountryCode in Country model
        /// </summary>
        /// <param name="id">CountryCode</param>
        /// <returns>True if requested CountryCode exists</returns>
        private bool CountryExists(string id)
        {
            return _context.Country.Any(e => e.CountryCode == id);
        }
    }
}
