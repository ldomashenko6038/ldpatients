﻿using LDPatients.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace LDPatients.Controllers
{
    /// <summary>
    ///  The controller handles and responds to user input.
    ///  Depending on requested action controller selects the correct view and provides data using DispensingUnit model to it.
    ///  Also, LDDispensingUnit controller enables all CRUD maintanence.
    /// </summary>
    public class LDDispensingUnitController : Controller
    {
        private readonly PatientsContext _context;

        public LDDispensingUnitController(PatientsContext context)
        {
            _context = context;
        }

        // GET: LDDispensingUnit
        /// <summary>
        /// The method returns all data for DispensingUnit model
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Index()
        {
            return View(await _context.DispensingUnit.ToListAsync());
        }

        // GET: LDDispensingUnit/Details/5
        /// <summary>
        /// The method returns details for requested DispensingCode
        /// </summary>
        /// <param name="id">DispensingCode</param>
        /// <returns>DispensingUnit if exists</returns>
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dispensingUnit = await _context.DispensingUnit
                .FirstOrDefaultAsync(m => m.DispensingCode == id);
            if (dispensingUnit == null)
            {
                return NotFound();
            }

            return View(dispensingUnit);
        }

        // GET: LDDispensingUnit/Create
        /// <summary>
        /// The method returns Create New DispensingUnit view
        /// </summary>
        /// <returns></returns>
        public IActionResult Create()
        {
            return View();
        }

        // POST: LDDispensingUnit/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// The method adds DispensingUnit to database
        /// </summary>
        /// <param name="dispensingUnit">DispensingUnit object</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("DispensingCode")] DispensingUnit dispensingUnit)
        {
            if (ModelState.IsValid)
            {
                _context.Add(dispensingUnit);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(dispensingUnit);
        }

        // GET: LDDispensingUnit/Edit/5
        /// <summary>
        /// The method returns edit view with data for requested DispensingCode
        /// </summary>
        /// <param name="id">DispensingCode</param>
        /// <returns></returns>
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dispensingUnit = await _context.DispensingUnit.FindAsync(id);
            if (dispensingUnit == null)
            {
                return NotFound();
            }
            return View(dispensingUnit);
        }

        // POST: LDDispensingUnit/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// The method edit requested dispensingUnit and save it in db
        /// </summary>
        /// <param name="id">DispensingCode</param>
        /// <param name="dispensingUnit">DispensingUnit object</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("DispensingCode")] DispensingUnit dispensingUnit)
        {
            if (id != dispensingUnit.DispensingCode)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(dispensingUnit);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DispensingUnitExists(dispensingUnit.DispensingCode))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(dispensingUnit);
        }

        // GET: LDDispensingUnit/Delete/5
        /// <summary>
        /// The method returns delete view with DispensingUnit model data
        /// </summary>
        /// <param name="id">DispensingCode</param>
        /// <returns></returns>
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dispensingUnit = await _context.DispensingUnit
                .FirstOrDefaultAsync(m => m.DispensingCode == id);
            if (dispensingUnit == null)
            {
                return NotFound();
            }

            return View(dispensingUnit);
        }

        // POST: LDDispensingUnit/Delete/5
        /// <summary>
        /// The method deletes the requested DispensingCode and save changes in db
        /// </summary>
        /// <param name="id">DispensingUnit's DispensingCode</param>
        /// <returns>Index view for LDDispensingUnit controller</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var dispensingUnit = await _context.DispensingUnit.FindAsync(id);
            _context.DispensingUnit.Remove(dispensingUnit);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        /// <summary>
        /// The method checks an existance of DispensingCode in DispensingUnit model
        /// </summary>
        /// <param name="id">DispensingCode</param>
        /// <returns>True if requested DispensingCode exists</returns>
        private bool DispensingUnitExists(string id)
        {
            return _context.DispensingUnit.Any(e => e.DispensingCode == id);
        }
    }
}
