﻿using LDPatients.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace LDPatients.Controllers
{
    /// <summary>
    ///  The controller handles and responds to user input.
    ///  Depending on requested action controller selects the correct view and provides data using MedicationType model to it.
    ///  Also, LDMedicationType controller enables all CRUD maintanence.
    /// </summary>
    public class LDMedicationTypeController : Controller
    {
        private readonly PatientsContext _context;

        public LDMedicationTypeController(PatientsContext context)
        {
            _context = context;
        }

        // GET: LDMedicationType
        /// <summary>
        /// The method returns all data for MedicationType model
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Index()
        {
            return View(await _context.MedicationType.ToListAsync());
        }

        // GET: LDMedicationType/Details/5
        /// <summary>
        /// The method returns details for requested MedicationTypeId
        /// </summary>
        /// <param name="id">MedicationTypeId</param>
        /// <returns>MedicationType if exists</returns>
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var medicationType = await _context.MedicationType
                .FirstOrDefaultAsync(m => m.MedicationTypeId == id);
            if (medicationType == null)
            {
                return NotFound();
            }

            return View(medicationType);
        }

        // GET: LDMedicationType/Create
        /// <summary>
        /// The method returns Create New MedicationType view
        /// </summary>
        /// <returns></returns>
        public IActionResult Create()
        {
            return View();
        }

        // POST: LDMedicationType/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// The method adds MedicationType to database
        /// </summary>
        /// <param name="medicationType">MedicationType object</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("MedicationTypeId,Name")] MedicationType medicationType)
        {
            if (ModelState.IsValid)
            {
                _context.Add(medicationType);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(medicationType);
        }

        // GET: LDMedicationType/Edit/5
        /// <summary>
        /// The method returns edit view with data for requested MedicationTypeId
        /// </summary>
        /// <param name="id">MedicationTypeId</param>
        /// <returns></returns>
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var medicationType = await _context.MedicationType.FindAsync(id);
            if (medicationType == null)
            {
                return NotFound();
            }
            return View(medicationType);
        }

        // POST: LDMedicationType/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// The method edit requested MedicationType and save it in db
        /// </summary>
        /// <param name="id">MedicationTypeId</param>
        /// <param name="medicationType">MedicationType object</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("MedicationTypeId,Name")] MedicationType medicationType)
        {
            if (id != medicationType.MedicationTypeId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(medicationType);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MedicationTypeExists(medicationType.MedicationTypeId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(medicationType);
        }

        // GET: LDMedicationType/Delete/5
        /// <summary>
        /// The method returns delete view with MedicationType model data
        /// </summary>
        /// <param name="id">MedicationTypeId</param>
        /// <returns></returns>
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var medicationType = await _context.MedicationType
                .FirstOrDefaultAsync(m => m.MedicationTypeId == id);
            if (medicationType == null)
            {
                return NotFound();
            }

            return View(medicationType);
        }

        // POST: LDMedicationType/Delete/5
        /// <summary>
        /// The method deletes the requested MedicationTypeId and save changes in db
        /// </summary>
        /// <param name="id">MedicationTypeId</param>
        /// <returns>Index view for LDMedicationType controller</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var medicationType = await _context.MedicationType.FindAsync(id);
            _context.MedicationType.Remove(medicationType);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        /// <summary>
        /// The method checks an existance of MedicationTypeId in MedicationType model
        /// </summary>
        /// <param name="id">MedicationTypeId</param>
        /// <returns>True if requested MedicationTypeId exists</returns>
        private bool MedicationTypeExists(int id)
        {
            return _context.MedicationType.Any(e => e.MedicationTypeId == id);
        }
    }
}
