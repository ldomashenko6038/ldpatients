﻿using LDPatients.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace LDPatients.Controllers
{
    /// <summary>
    ///  The controller handles and responds to user input.
    ///  Depending on requested action controller selects the correct view and provides data using DiagnosisCategory model to it.
    ///  Also, LDDiagnosisCategory controller enables all CRUD maintanence.
    /// </summary>
    public class LDDiagnosisCategoryController : Controller
    {
        private readonly PatientsContext _context;

        public LDDiagnosisCategoryController(PatientsContext context)
        {
            _context = context;
        }

        // GET: LDDiagnosisCategory
        /// <summary>
        /// The method returns all data for DiagnosisCategory model
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Index()
        {
            return View(await _context.DiagnosisCategory.ToListAsync());
        }

        // GET: LDDiagnosisCategory/Details/5
        /// <summary>
        /// The method returns details for requested Id
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>DiagnosisCategory if exists</returns>
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var diagnosisCategory = await _context.DiagnosisCategory
                .FirstOrDefaultAsync(m => m.Id == id);
            if (diagnosisCategory == null)
            {
                return NotFound();
            }

            return View(diagnosisCategory);
        }

        // GET: LDDiagnosisCategory/Create
        /// <summary>
        /// The method returns Create New CategoryDiagnosis view
        /// </summary>
        /// <returns></returns>
        public IActionResult Create()
        {
            return View();
        }

        // POST: LDDiagnosisCategory/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// The method adds diagnosisCategory to database
        /// </summary>
        /// <param name="diagnosisCategory">DiagnosisCategory object</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name")] DiagnosisCategory diagnosisCategory)
        {
            if (ModelState.IsValid)
            {
                _context.Add(diagnosisCategory);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(diagnosisCategory);
        }

        // GET: LDDiagnosisCategory/Edit/5
        /// <summary>
        /// The method returns edit view with data for requested Id
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var diagnosisCategory = await _context.DiagnosisCategory.FindAsync(id);
            if (diagnosisCategory == null)
            {
                return NotFound();
            }
            return View(diagnosisCategory);
        }

        // POST: LDDiagnosisCategory/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// The method edit requested diagnosisCategory and save it in db
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="diagnosisCategory">diagnosisCategory object</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name")] DiagnosisCategory diagnosisCategory)
        {
            if (id != diagnosisCategory.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(diagnosisCategory);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DiagnosisCategoryExists(diagnosisCategory.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(diagnosisCategory);
        }

        // GET: LDDiagnosisCategory/Delete/5
        /// <summary>
        /// The method returns delete view with DiagnosisCategory model data
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var diagnosisCategory = await _context.DiagnosisCategory
                .FirstOrDefaultAsync(m => m.Id == id);
            if (diagnosisCategory == null)
            {
                return NotFound();
            }

            return View(diagnosisCategory);
        }

        // POST: LDDiagnosisCategory/Delete/5
        /// <summary>
        /// The method deletes the requested Id and save changes in db
        /// </summary>
        /// <param name="id">DiagnosisCategory's Id</param>
        /// <returns>Index view for LDDiagnosisCategory controller</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var diagnosisCategory = await _context.DiagnosisCategory.FindAsync(id);
            _context.DiagnosisCategory.Remove(diagnosisCategory);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        /// <summary>
        /// The method checks an existance of Id in DiagnosisCategory model
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>True if requested Id exists</returns>
        private bool DiagnosisCategoryExists(int id)
        {
            return _context.DiagnosisCategory.Any(e => e.Id == id);
        }
    }
}
