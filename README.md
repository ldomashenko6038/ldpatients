﻿Simple Web Application for Online Clinic access.
 
This web application is used for providing access to clinic 
for specific sections: Country, ConcentrationUnit, DiagnosisiCategory,DispensingUnit and MedicationType.

Below are the steps required to get this working on a base windows system: 
• Install all required dependencies 
• Install and Configure Database 
• Start Database Service 
• Install and Configure Web Server 
• Start Web Server 
• Open a browser and go to URL

License:
Leila Domashenko Copyright (C) 2020. See LICENSE for details.